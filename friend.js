// prepare
if (Drupal.jsEnabled){

	var obj = null;
	
	$(document).ready(function(){

		// Collapse everything but the first menu:
		$(".slideMenu").find(".slideSubMenu").slideUp(1);
		// Expand or collapse:
		$(".slideMenu").click(function(){
			$(this).find(".slideSubMenu").slideToggle("fast");
		});

		$(".corkboard-header").click(function() {
				$(".corkboard-data").slideToggle("slow");
			}
		);
        $('.friend-user-box').click(function(){
            var id = $(this).attr('target_id');
            $('#' + id).slideToggle("slow");
        });

        $('#ajax-get').click(function(){

            var loadEquipment = function(data){
                var result = Drupal.parseJson(data);
                $('#' + result['target_id']).html(result['output']);
                $('#' + result['target_id']).slideToggle('fast');
            }
            $.get(this.href, null, loadEquipment);
            return false;
        })

        // toggle the search result
        $('a.toggle').click(function(){
            var id = $(this).attr('target_id');
            $('#' + id).slideToggle();
			return false;
        });

	
	});
}
