<?php

/***********************************************************************************************
 * Form builder; Configure friend settings for this site
 * 
 * @see system_settings_form().
 */
function friend_admin_settings(){

	// Friend email setttings
	$form['email'] = array(
		'#type'		=> 'fieldset',
		'#title'	=> t("Friend e-mail settings"),
		'#description' 	=> t("Drupal sends emails whenever a user request to add a friend to his/her friends list or when user invites a friend to your site. Using a simple set of content templates, notification e-mails can be customized to fit the specific needs of your site."),
	);

	$email_token_help = t("Available variables are: !username, !friend_name, !site, !uri, !uri_request, !mailto, !message, ! !date ");

	$form['email']['friend_request'] = array(
		'#type'	=> 'fieldset',
		'#title' => t("Request member to be added to friends list."),
		'#collapsible'	=> TRUE,
		'#collapsed' 	=> true,
		'#description'	=> t("Customize request email sent to member. $email_token_help"),
	);

	$form['email']['friend_request']['friend_request_subject'] = array(
		'#type'	=> 'textfield',
		'#title' => t("Subject"),
		'#default_value'	=> _friend_mail_text('friend_request_subject'),
	);
	$form['email']['friend_request']['friend_request_body'] = array(
		'#type'	=> 'textarea',
		'#title' => t("Body"),
		'#default_value'	=> _friend_mail_text('friend_request_body'),
		'#rows'	=> 10,
	);

	$form['email']['friend_invite'] = array(
		'#type'	=> 'fieldset',
		'#title' => t("Invite friend to join site."),
		'#collapsible'	=> TRUE,
		'#collapsed' 	=> true,
		'#description'	=> t("Customize invite email sent to member. $email_token_help"),
	);

	$form['email']['friend_invite']['friend_invite_subject'] = array(
		'#type'	=> 'textfield',
		'#title' => t("Subject"),
		'#default_value'	=> _friend_mail_text('friend_invite_subject'),
	);
	$form['email']['friend_invite']['friend_invite_body'] = array(
		'#type'	=> 'textarea',
		'#title' => t("Body"),
		'#default_value'	=> _friend_mail_text('friend_invite_body'),
		'#rows'	=> 10,
	);


	return system_settings_form($form);

};
