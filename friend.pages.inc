<?php

/**
 * @file
 * Page callback file for the friend module
 */
/***********************************************************************
 * Menu callback 
 * 	Friends list section
 */
function friend_corktocork($friend_id = null){
global $user; // this is the view looking at the board of $friend_id

	if (!friendapi_is_friend($user->uid,$friend_id)){
		drupal_set_message(t('Not your friend'));
		return false;
	};

	drupal_set_title(t('My Cork to Cork with'));
	$max = variable_get('friend_max_cork',10);

	$output = theme('friend_box',$friend_id);
	$output .= drupal_get_form('friend_corkboard_form',$friend_id);

	// now grab all the wals from but $user and $friend_id
	$SQL = "SELECT CB.cbid
			FROM {friend_corkboard} CB
			WHERE 
					(CB.uid = %d AND CB.fid = %d)
					OR
					(CB.uid = %d AND CB.fid = %d)
			ORDER BY cbid DESC
	";
	$result = pager_query($SQL,$max,0,null,
				$friend_id,$user->uid,
				$user->uid,$friend_id);

	$output .= theme('pager', NULL, $max);
	$output .= '<div id="cork_output">';
	while($record = db_fetch_object($result)){
		$output .= theme('friend_cb_message',$record->cbid);
	};
	$output .= '</div>';

	return $output;

}
/***********************************************************************
 * Menu callback 
 * 	Friends list section
 */
function friend_lists($list_id = null){
	// show all members in this lest and show the form for the user to 
	// add new friends to this list.
	global $user;

	$list_name = friendapi_list_name($list_id); 
	$title = drupal_get_title();
	drupal_set_title($title .' - '. 
					'<span class="friend_list_title">'.$list_name.'</span>');

	$form .= drupal_get_form('friend_list_addfriend_form',$list_id);
	$list_form = drupal_get_form('friend_list_form');
	$friend_lists = _friend_lists($user->uid); // fnd in friend.module
	$friends  = friendapi_list_member($user->uid,$list_id); 

	// setup the delete list link
	$navbar .= '<li />'.l(t('delete this list'),'friend/lists/del/'.$list_id);

	$html = '
	<div id="friend-list-member">
		'.$form.'
	</div>
	<div id="friend-list-list">
		<div id="friend_list">'.$friend_lists.'</div>
		'.$list_form.'
	</div>
	<div id="friend-list-navbar">
	<ul class="links">
		'.$navbar.'
	</ul>
	</div>
	<div id="friend-list">
		'.$friends.'
	</div>
	';


	return $html;
}
/***********************************************************************
 * Menu callback 
 * 	Privacy Settings for the user
 */
function friend_privacy(){

	$output = drupal_get_form('friend_privacy_form');

	return $output;
}
function friend_privacy_form(){
global $user;

	$lists = array('0'=>"All Friends");
	$lists += friendapi_get_lists($user->uid);


	$SQL = "SELECT * FROM {friend_privacy} WHERE owner_id = %d";
	$result = db_query($SQL,$user->uid);
	$access_list = array();
	while($record = db_fetch_object($result)){
		$access_list[$record->list_id][$record->content_name] = array(
							'content_name' => $record->content_name,
							'rights'		=> $record->rights,
					);
	};
	// Render list/permission overview:
	$options = array();
	foreach(module_list(FALSE, FALSE, TRUE) as $module){
		if ($permissions = module_invoke($module, 'friend_access')){

			foreach($permissions as $index => $perm){
				$options[$perm['name']] = '';
				$form['permission'][$perm['name']] = array('#value' => t($perm['label']));
				foreach($lists as $lid => $name){
					// Build arrays for the check boxeds for each list	
					if ($access_list[$lid][$perm['name']]['content_name'] == $perm['name']){
						$rights = $access_list[$lid][$perm['name']]['rights'];

						if ($rights == 1){
							$status[$lid][] = $perm['name'];
						};
					};
				};
			};
		};
	};

	reset($lists);
	foreach($lists as $lid => $name){
		$form['checkboxes'][$lid] = array('#type' => 'checkboxes', '#options' => $options,'#default_value' => isset($status[$lid]) ? $status[$lid] : array());
		$form['list_names'][$lid] = array('#value' => $name, '#tree' => TRUE);
	};
	
	
	$form['submit'] = array(
		'#type'	=> 'submit',
		'#value'	=> t('Update'),
	);

	return $form;
}
function theme_friend_privacy_form($form){
global $user;

	$lists = array('friend_all'=>"All Friends");
	$lists += friendapi_get_lists($user->uid);

	foreach(element_children($form['permission']) as $key){
		// Don't take form control structures	
		if(is_array($form['permission'][$key])){
			$row = array();
			
			$row[] = array(
				'data' => drupal_render($form['permission'][$key]),
			);
			foreach(element_children($form['checkboxes']) as $lid){
				if (is_array($form['checkboxes'][$lid])){
					$row[] = array(
						'data'	=> drupal_render($form['checkboxes'][$lid][$key]), 'class' => 'checkbox');
				};
			};
			$rows[] = $row;
		}
	};
	$header[] = (t('Content'));
	foreach(element_children($form['list_names']) as $lid){
		$header[] = array(
			'data' => drupal_render($form['list_names'][$lid]), 
			'class'=>'checkbox'
		);
	};

	$output = theme('table',$header,$rows);
	$output .= drupal_render($form);

	return $output;
}
function friend_privacy_form_submit($form,&$form_state){
global $user;
	$SQL = "DELETE FROM {friend_privacy} WHERE owner_id = %d";
	db_query($SQL,$user->uid);

	$values = $form_state['values'];
	while(list($lid,$checkboxes) = each($values)){
		if(is_array($checkboxes)){
			// okay to insert this.
			while(list($content_name,$value) = each($checkboxes)){
				$SQL = "INSERT INTO {friend_privacy}
						(owner_id,list_id,viewer_id,network_id,
						 content_name,rights)
						VALUES
						(%d,%d,%d,%d,'%s',%d)
				";
				if ($value != '0'){
					db_query($SQL,$user->uid,$lid,0,0,$content_name,1);
				}else{
					db_query($SQL,$user->uid,$lid,0,0,$content_name,0);
				};
			};
		};
	};
	
	drupal_set_message(t('Privacy Settings Updated'));

}
/***********************************************************************
 * Menu callback 
 * 	provide a list of friends online.
 */
function friend_online(){
	global $user; 	// current logged in user.

	// check who's online and only show friends
	$SQL = "SELECT DISTINCT S.uid
			FROM {sessions} S 
				INNER JOIN {friend} F  on S.uid = F.uid
			WHERE S.timestamp >= %d	 AND S.uid <> %d
	";
	$interval = time() - variable_get('friend_seconds_online', 900);
	$result = db_query($SQL,$interval,$user->uid,$user->uid);
	$online_friends_count = 0;
	while ($account = db_fetch_object($result)){
		$output .= theme('friend_box', $account->uid);
		$online_friends_count++;
	};
	
	$message .= t('There are currently %friends online.', array('%friends'	=> format_plural($online_friends_count,' 1 friend', '@count friends')));
	

	if ($online_friends_count){
		return $message.$output;
	}else{
		return t('You have no friends online.');
	};	
}
/***********************************************************************
 * Menu callback 
 * 	friend fro user list
 */
function friend_invite_page(){
global $user;

	$output = drupal_get_form('friend_invite_form');

	// Pending external invites	
	$SQL = "SELECT * FROM {friend_invited
			WHERE uid = %d
	";
	$result = db_query($SQL,$user->uid);
	$rows = array();
	while($record = db_fetch_object($result)){
		$rows[] = array(
			array('data' => date('M d, Y',$record->timestamp)),
			array('data' => $record->email),
		);
	};

	$output .= t("Pending External Invites");
	$output .= theme('table',array(t('Date'),t('Email')),$rows);

	return $output;

};
/***********************************************************************
 * Menu callback 
 *  external invite form
 */
function friend_invite_form(){
	global $user;

	$form['from'] = array(
		'#type'	=> 'item',
		'#title'	=> t('From'),
		'#value'	=> t("$user->name &lt;$user->mail&gt;"),
	);
	$form['send_to'] = array(
		'#type'	=> 'textfield',
		'#title'	=> t('To'),
		'#description'	=> t("(use commas to separate emails)"),
		'#required'	=> true,
	);
	$form['message_body'] = array(
		'#type'	=> 'textarea',
		'#title'	=> t('Message'),
		'#rows'		=> 4,
		'#description'	=> t("(optional)"),
	);
	$form['submit'] = array(
		'#type'	=> 'submit',
		'#value'	=> t("Invite"),
	);

	return $form;

};
/***********************************************************************
 * hook_form_validate()
 */
function friend_invite_form_validate($form_id, $form_state){

/*
		if (!valid_email_address($form_state['values']['send_to'])){
			form_set_error('send_to',t($form_state['values']['send_to'] ." Not a valid email address!"));
		};
*/
};
/***********************************************************************
 * hook_form_submit()
 * enter the email into a pending table and then send an email to the 
 * friend.
 */
function friend_invite_form_submit($form_id, $form_state){
	global $user;

	// don't forget that users can enter in more than one email address
	// which are seperated by comas.
	$emails = array();
	$emails = explode(",",$form_state['values']['send_to']);
	while (list($index,$email) = each($emails)){

		$email = trim($email);
		if (valid_email_address($email)){
			$SQL = "DELETE FROM {friend_invited} WHERE uid=%d AND email ='%s'";
			db_query($SQL,$user->uid,$email);
			$SQL = "INSERT INTO {friend_invited} (uid,email,timestamp)
					VALUES (%d,'%s',%d)
			";
			$result = db_query($SQL,$user->uid,$email,time());
			if($result){
				$params['message'] = $form_state['values']['message_body'];
				drupal_mail('friend','friend_invite',$email,
										language_default(),$params,$user->mail);
				drupal_set_message("Email has been sent.");
			}else{
				drupal_set_message("There was an error senting invite to $email.","error");
			};
		};//end if
	};

}
/***********************************************************************
 * Menu callback 
 * 	removes friend fro user list
 */
function friend_remove($friend_id){
	global $user;

	// First delete from any  lists
	$SQL = "SELECT lid FROM {friend_list} WHERE uid = %d";
	$result = db_query($SQL,$user->uid);
	while ($record = db_fetch_object($result)){
		$SQL = "DELETE FROM {friend_list_member}
				WHERE lid = %d AND uid = %d
		";
		db_query($SQL,$record->lid, $friend_id);
	};
	
	$SQL = "DELETE FROM {friend} WHERE uid=%d AND fid=%d";
	$result = db_query($SQL,$user->uid,$friend_id);
	// remove reciprocal link
	$result = db_query($SQL,$friend_id,$user->uid);
	if ($result){
		module_invoke_all('friend','remove',$user->uid,$friend_id);
		drupal_set_message("Friend removed");
	}else{
		drupal_set_message("Error in removing friend");
	};
	drupal_goto(); // to back to where we came from.

};
/***********************************************************************
 * Menu callback 
 * 	accept the request from $friend_id
 *
 * @param $friend_id
 *	the user making the request
 * @param $code
 *	1 = accept 0 = reject request for friend
 */
function friend_respond($friend_id,$code){
	global $user; // current user
	// request has been accepted, set the status flag to FRIEND_FRIEND
	// and insert a reciprical friend record

	if ($code == 1){
		// request has been accepted so insert the friend
		$SQL = "UPDATE {friend} SET status = %d WHERE uid=%d AND fid=%d";
		$result = db_query($SQL, FRIEND_FRIEND, $friend_id, $user->uid);
		$SQL = "INSERT INTO {friend}
				(uid,fid,status,timestamp)
				VALUES
				(%d,%d,%d,%d)
		";
		$result = db_query($SQL,$user->uid,$friend_id,FRIEND_FRIEND,time());
		if ($result){
			// invoke friend
			module_invoke_all('friend','insert',$user->uid,$friend_id);
			$friend = db_fetch_object(db_query("SELECT name FROM {users} WHERE uid=%d",$friend_id));
			$variables = array(
				'%user' => $user->name,
				'%friend' => $friend->name,
			);
			watchdog('friend','%user and %friend are now friends.',$variables,WATCHDOG_INFO);
			drupal_set_message("Friend has been added");
		}else{
			drupal_set_message("Error in adding your friend");
		};
	}else{
		$SQL = "DELETE FROM {friend} WHERE uid=%d AND fid=%d";
		$result = db_query($SQL,$friend_id,$user->uid);
		if ($result){
			drupal_set_message("Friend has been rejected");
		}else{
			drupal_set_message("Error in adding your friend");
		};

	};
	drupal_goto();
}
/***********************************************************************
 * Menu callback to view friends current user has requested
 * grab any request for current user that is logged in.
 */
function friend_pending(){
	global $user;

	$output = t("<p>These are friends you've requested to add to your friend list and waiting for their approval.</p> ");

	$SQL = "SELECT fid FROM {friend} WHERE uid = %d AND status = %d";
	$result = db_query($SQL,$user->uid, FRIEND_PENDING);
	$cnt = 0;
	while($record = db_fetch_object($result)){
		$cnt++;
		$output .= theme('friend_box',$record->fid);
	};
	if ($cnt == 0){
		$output .= "None Found";
	};

	return $output;
};
/***********************************************************************
 * Menu callback to view current users list of friends
 * grab any request for current user that is logged in.
 */
function friend_get_requests(){
	global $user;

	$output = t("These are friends asking you to add to their friend list.<br />");

	$SQL = "SELECT uid FROM {friend} WHERE fid = %d AND status = %d";
	$result = db_query($SQL,$user->uid, FRIEND_PENDING);
	$cnt = 0;
	while($record = db_fetch_object($result)){
		$cnt++;
		$output .= theme('friend_box',$record->uid);
		$output .= l("Accept Request","friend/respond/$record->uid/1/",array('query'=>drupal_get_destination()));
		$output .= " | ";
		$output .= l("Reject Request","friend/respond/$record->uid/0/",array('query'=>drupal_get_destination()));
	};
	if ($cnt == 0){
		$output .= "No pending requests";
	};

	return $output;
};
/***********************************************************************
 * Menu callback to view current users list of friends
 */
function friend_myfriends(){
	global $user;
	drupal_set_title(t("My Friends"));	
	$max_listing = variable_get('friend_max_friend_list',20);

	$friend_requests = _friend_cnt_requests($user->uid);
	if ($friend_requests > 0){
        $respond = l(t('Respond'),'friend/requests');
    	drupal_set_message(t("You have @pending pending requests ... ".$respond,array('@pending'=>$friend_requests )));
	};

	$SQL = "SELECT fid FROM {friend} WHERE uid =%d AND status=%d";
	//$result = db_query($SQL,$user->uid,FRIEND_FRIEND);
	$result = pager_query($SQL,$max_listing,0,null,$user->uid,FRIEND_FRIEND);
	$cnt=0;
	while($record = db_fetch_object($result)){
		$cnt++;
		$output .=  theme('friend_box',$record->fid);
	};
	$paging = theme('pager',NULL,$max_listing);
	$output = $paging . $output . $paging;
	$output = format_plural($cnt, 'You have 1 friend', 'You have @count friends') ."<br />".$output;

	$list_form = drupal_get_form('friend_list_form');
	// get the list create by the current user.
	$friend_lists = _friend_lists($user->uid); // fnd in friend.module
	$page = '
	<div id="friend-display">
	<div id="friend_left">'.$output.'</div>
		<div id="friend_right">
			<div id="friend_list">'.$friend_lists.'</div>
				'.$list_form.'
		</div>
	</div>
	';


	return $page;	
};
/***********************************************************************
 * Menu callback to view current users list of friends
 */
function friend_request($account){
	global $user;

	$output .=  theme('friend_box',$account->uid);
	$output .= t("Add @name as friend? ",array('@name' => $account->name));
	$output .= l(t("YES"),"friend/add/$account->uid",
						array('query'=>drupal_get_destination())) . " | ";
	$output .= l(t("CANCEL"),"user/$account->uid",
						array('query'=>drupal_get_destination())
						);
	return $output;
}
/***********************************************************************
 * Menu callback to add friend to list and send request
 */
function friend_add($friend){
	global $user;

	// first insert the new friend into the friend table
	// * add friend as pending
	$SQL = "INSERT INTO {friend}
			(uid,fid,status,timestamp)
			VALUES
			(%d,%d,%d,%d)
	";
	if ($user->uid > 0 && $friend->uid > 0){
		// first check to see if user has already invided
		// if the record already exists then there's no point in inserting
		// again.
		$SQL_TEMP = "SELECT COUNT(fid) as found FROM {friend} 
					WHERE uid=%d AND fid=%d";
		$record = db_fetch_object(db_query($SQL_TEMP,$user->uid,$friend->uid));
											
		if ($record->found == 0){ 
			$result = db_query($SQL,$user->uid,$friend->uid,
									FRIEND_PENDING,time());
		}else{
			$result = true;
		};

		// now email the new friend
		if ($result){
			$mail_result = _friend_mail_request($friend);
		};
		if($mail_result){
			drupal_set_message(t("Request sent to @name",array('@name'=> $friend->name)));
			drupal_goto();
		}else{
			drupal_set_message(t("There was an erorr sending request to ") . $friend->name, 'error');
			$output = t("Error in request: ").$mail_result;
		};
	};

	return $output;	
}
