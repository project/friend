<?php
/***********************************************************************
 * Menu callback 
 *  ajax call when user adds cork messages to a friend's board
 */
function friend_current_online(){


    global $user;   // current logged in user.

    // check who's online and only show friends
    $SQL = "SELECT DISTINCT S.uid,U.name
            FROM {sessions} S 
                INNER JOIN {friend} F  on S.uid = F.uid
                INNER JOIN {users} U  on F.uid = U.uid
            WHERE S.timestamp >= %d  AND S.uid <> %d
				AND S.uid > 0
    ";
    $interval = time() - variable_get('friend_seconds_online', 900);
    $result = db_query($SQL,$interval,$user->uid);
    $online_friends_count = 0;
    while ($account = db_fetch_object($result)){
        $online_friends .= l($account->name,'user/'.$account->uid );
    	$online_friends_count++;
    };
	$output = "$online_friends_count ".t('friend(s) online')." <br />". $online_friends;

    print drupal_to_js(array(
            'status'    => TRUE,
            'target_id'    => 'friend_online',
            'output'  => $output,
        )
    );
    exit();
}
/***********************************************************************
 * Menu callback 
 *  ajax call when user adds cork messages to a friend's board
 */
function friend_cork_add(){
    global $user; // person doing the posting.

    $message = $_POST['corkboard-message'];
    $uid = $_POST['uid']; // owner of the corkboard being posted on.

    // first insert
    if (strlen($message) > 0){
        $SQL = "INSERT INTO {friend_corkboard} (uid,fid,message,timestamp)
                VALUES (%d,%d,'%s',%d)
        ";
        $result = db_query($SQL,$uid,$user->uid,$message,time());
        $new_cbid = db_last_insert_id("friend_corkboard","cbid");
        $variables = array('cbid'=>$new_cbid);
        // invoke modules who want to hook into friend.
        module_invoke_all('friend','post_cork',$user->uid,$uid,$variables);
    };

    $max = variable_get('friend_max_cork',10);
    $SQL = "SELECT CB.cbid
            FROM {friend_corkboard} CB
                INNER JOIN {users} U on CB.fid = U.uid
            WHERE CB.uid=%d
                    AND CB.cbid = %d
            ORDER BY timestamp DESC";
    $result = db_query_range($SQL,$uid,$new_cbid,0,$max);

    $record = db_fetch_object($result);
    $output .= theme('friend_cb_message',$record->cbid);

    print drupal_to_js(array(
            'status'    => TRUE,
            'data'  => $output,
        )
    );
    exit();

}
