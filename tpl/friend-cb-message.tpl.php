<?php

/**
 * @file friend-cb-message.tpl.php
 * Default theme implementation to present the corkboard message on the user's profile page.
 * 
 * Available variables:
 *  $corkboard->html_picture : user profile image
 *  $corkboard->html_name : link to user profile page
 *  $corkboard->message : corkboard message
 *  $corkboard->uid : user id of poster
 *  $corkboard->cbid : corkboard id
 *  $corkboard->cb_owner : owner of the corkboard
 */
// un-comment the next line to see variables
// print_r($corkboard);
global $user;
if ($corkboard->cb_owner == $user->uid){
	$del_link = l(t('delete'),'friend/cork_del/'.$corkboard->cbid,
				array('query' => 'destination=user/'.$corkboard->cb_owner)
			);
	
};
if ($corkboard->cb_owner == $user->uid &&
	$corkboard->uid != $user->uid
	){
	$corktocork_link = l(t('cork-to-cork'),'friend/corktocork/'.$corkboard->uid);
};
$links = $corktocork_link;
if($del_link){
	$links .= ' - '.$del_link;
};

?>
<table class="corkboard">
<tr>
	<td class="corkboard-image">
		<div class="cb_picture"> 
		<?php print $corkboard->html_picture; ?> 
		</div>
	</td>
	<td valign="top">
		<div class="cb_name"> <?php print $corkboard->html_name; ?> wrote 
		<span class="cb_datetime">at <?php print $corkboard->html_datetime; ?></span>
		</div>
		<div class="cb_message"><?php print $corkboard->message; ?></div>
	<div class="cb_action"> <?php print $links; ?></div>
	</td>
</tr>
</table>
<div style="clear:both"></div>
