<?php

/**
 * @file friend-box.tpl.php
 * Default theme implementation to present the friend
 * 
 * Available variables:
 *	user print_r($friend) to see available variables;
 */
// un comment next line to see vars
//print_r($friend);
?>
<div class="friend-box">
		<div class="fb_picture"> <?php print $friend->html_picture; ?> </div>
		<div class="fb_name"> 
			<div class="fb_menu"><?php print $action_menu; ?></div>
				<?php print $friend->html_name; ?> 
		</div>
		<div class="fb_body"> <?php print $friend->html_member_since; ?> </div>
</div>
