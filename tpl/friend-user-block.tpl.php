<?php
/*
 * To check for all available data within $blocks, use the code below.
 *

	$block['title']; 
	$block['id']; 
	$block['value']; 
	
*/
//drupal_set_message(print_r($blocks,1));
?>
<div id="friend-block-wrapper">
	<?php if ($block['title']) : ?>
	<div class="friend-user-box" target_id="<?php print $block['id'];?>"><?php print $block['title']; ?></div>
	<?php endif;?>
	<div id="<?php print $block['id']; ?>">
		<table>
		<tr>
			<td>
			<?php print $block['value']?> 
			</td>
		</tr>
		</table>
	</div>
</div>
