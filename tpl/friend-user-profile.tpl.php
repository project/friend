<?php
/*
 * To check for all available data within $blocks, use the code below.
 *

	$blocks->friend_list; // list of all friends
	$blocks->friend_mutual; // list of all friends
	$blocks->friend_groups; // list of all friends
	
*/
//drupal_set_message(print_r($blocks,1));
?>

<table id="friend-profile-table">
    <tr>
        <td valign="top" id="friend-profile-left">
        	<?php print $blocks['friend_user_info']; ?>
        	<?php print $blocks['friend_list']; ?>
        	<?php print $blocks['friend_mutual']; ?>
        	<?php print $blocks['friend_groups']; ?>
        </td>
        <td valign="top" id="friend-profile-right">
        	<?php print $blocks['notice_mini_feed']; ?>
        	<?php print $blocks['friend_corkboard']; ?>
        </td>
    </tr>
</table>
