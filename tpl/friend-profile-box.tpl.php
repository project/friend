<?php

/**
 * @file friend-box.tpl.php
 * Default theme implementation to present the friend
 * 
 * Available variables:
 *	user print_r($friend) to see available variables;
 *	user print_r($attributes) to see available variables;
 */
// un comment next line to see vars
//print_r($friend);
if (!empty($friend->picture) && file_exists($friend->picture)){
	$picture = file_create_url($friend->picture);
}elseif (variable_get('user_picture_default','')){
	$picture = variable_get('user_picture_default', '');
};
if (isset($picture)){
	$alt = t("@user's picture", array('@user'=> $friend->name ? $friend->name : variable_get('anonymous',t('Anonymous'))));
	$profile_img = theme('image', $picture, $alt, $alt, '', FALSE);
   	$attributes = array(
   		'attributes'    => array(
       		'title' => t('View user profile.')
    	),
       	'html'  => TRUE
       );
	$profile_img = l($profile_img,"user/$friend->fid",$attributes);
};

$username = l($friend->name,'user/'.$friend->fid);
?>
<div class="friend-profile-box">
		<div class="fb_picture"> <?php print $profile_img; ?> </div>
		<div class="friend-profile-name"> <?php print $username; ?> </div>
</div>
