<?php

/************************************************************************
 * Preprocess template for corkboard message.
 */
function template_preprocess_friend_user_profile(&$variables){

	$blocks = $variables['blocks'];
	$variables['blocks'] = array();
	while(list($index,$block) = each($blocks)){
		$id = $block['id'];
		$variables['blocks'][$id] = theme('friend_user_block',$block);
	};
//	drupal_set_message(print_r($variables,1));
}
/************************************************************************
 * Preprocess template for corkboard message.
 */
function template_preprocess_friend_cb_message(&$variables){
global $user;

    $SQL = "SELECT U.name,U.picture,U.uid,CB.message,CB.cbid,
                    CB.timestamp, CB.uid as cb_owner
            FROM {friend_corkboard} CB 
                INNER JOIN {users} U on CB.fid = U.uid
            WHERE CB.cbid = %d
    ";
    $account = db_fetch_object(db_query($SQL,$variables['corkboard_id']));
   
    $variables['picture'] = '';
    if (variable_get('user_pictures', 0)){
        if (!empty($account->picture) && file_exists($account->picture)){
            $picture = file_create_url($account->picture);
        }elseif(variable_get('user_picture_default','')){
            $picture = variable_get('user_picture_default','');
        };
    }

    if (isset($picture)){
        $alt = t("@user's picture", array('@user' => $account->name ? $account->name : variable_get('anonymous', t('Anonymous'))));
        $variables['picture'] = theme('image',$picture, $alt, $alt, '', FALSE);
        if (!empty($account->uid) && user_access('access user profiles')){
            $attributes = array(
                'title' => t("View user profile."),
                'html'  => TRUE,
            );
            $profile_img = l($variables['picture'], "user/$account->uid", $attributes);
        };
    };

    $account->html_picture = $profile_img;
    $account->html_name = l($account->name,"user/$account->uid");
    $account->html_datetime = date('g:ia \o\n F jS, Y',$account->timestamp);
    $variables['corkboard'] = $account;
    $variables['current_user'] = $user->uid;

};
/************************************************************************
 * Preprocess template for friend
 */
function template_preprocess_friend_box(&$variables){

    // contains where the function was called.
    $fn_attributes = $variables['attributes'];

    $SQL = "SELECT U.picture,U.uid,U.name,U.created
            FROM {users} U
            WHERE U.uid = %d
    ";
    $friend = db_fetch_object(db_query($SQL,$variables['friend_id']));
    if (variable_get('user_pictures', 0)){
        if (!empty($friend->picture) && file_exists($friend->picture)){
            $picture = file_create_url($friend->picture);
        }elseif (variable_get('user_picture_default','')){
            $picture = variable_get('user_picture_default', '');
        };

        if (isset($picture)){
            $alt = t("@user's picture", array('@user'=> $friend->name ? $friend->name : variable_get('anonymous',t('Anonymous'))));
            $profile_img = theme('image', $picture, $alt, $alt, '', FALSE);
            $attributes = array(
                'attributes'    => array(
                    'title' => t('View user profile.')
                ),
                'html'  => TRUE
            );
            $profile_img = l($profile_img,"user/$friend->uid",$attributes);
        };
    };
    $friend->html_picture = $profile_img;
    $friend->html_name = l($friend->name,"user/$friend->uid");
    $friend->html_member_since = t("Member for ").format_interval(time() - $friend->created);
    $variables['friend'] = $friend;
    $variables['action_menu'] = _friend_action_link($friend,$fn_attributes);
};


?>
