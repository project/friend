<?php
/************************************************************************
 * Determine if the current user viewing has access to view the owner of the
 * content.
 *
 * @param $viewer_id
 * 	the user that want's to view the content
 * @param $owner_id
 * 	the owner of the content.
 * @return
 *	true if current user has access
 */
function friendapi_has_access($viewer_id,$owner_id,$content_name){

	// if user is admin then return ture to all content
	if (user_access("administer users")){
		return true;
	};
	// get all list belonging to the 
	if (friendapi_is_friend($owner_id,$viewer_id)){

		// first check to see if there are any record for the content
		// if it doesn't exists, then the owner has not set the privacy
		// settings yet. so allow all friend to access it by defualt.
		$SQL = "SELECT * FROM {friend_privacy} 
				WHERE content_name LIKE '%s'
					AND owner_id = %d
				";
		$result = db_query($SQL,$content_name,$owner_id);
		$access_list = array();
		while($record = db_fetch_object($result)){
			$access_list[$record->list_id] = array(
						'content_name'	=> $record->content_name,
						'rights'		=> $record->rights
			);
		};

		if (count($access_list) > 0){
			// first check if list_id = 0 -> all friends 
			if ($access_list[0]['rights'] == 1){
				// all friends have access to this content
				return true;
			}
			// okay, now grab all the list that the viewer_id belongs to 
			// in the owners lists
			$SQL = "SELECT L.* 
					FROM {friend_list} L
						INNER JOIN {friend_list_member} LM on L.lid = LM.lid
					WHERE L.uid = %d AND LM.uid = %d
			";
			$result = db_query($SQL,$owner_id,$viewer_id);
			$lists = array();
			while($record = db_fetch_object($result)){
				$lists[$record->lid]=$record->lid;
			};
			if (count($lists) > 0){
				foreach ($lists as $lid){
					if ($access_list[$lid]['rights'] == 1){
						return true;
					};
				};
				return false;
			}else{
				// view doesn't belong to any of owner's list so deney access.
				return false;
			};
		}else{
			// user has not setup privacy settings yet so default all friends 
			// are able to view .
			return true;
		};

	}else{
		return false;
	};

	

}
/************************************************************************
 *  return an array of list belonging to uid
 *
 * @param $uid
 * 	the user_id 
 * @return
 *	array of list belonging to the uid
 */
function friendapi_get_lists($uid=null){
	$SQL = "SELECT * FROM {friend_list} WHERE uid = %d";

	$result = db_query($SQL,$uid);
	$lists = array();
	while($list = db_fetch_object($result)){
		$lists[$list->lid] = $list->list_name;
	};

	return $lists;
}
/************************************************************************
 *  delete list and all members in that list.
 *
 * @param $lid
 * 	the id of the list
 * @return
 *	true
 */
function friendapi_lists_del($lid=null){
global $user;
	// first delete the list ... if that was okay
	// the delete all the members of that list. 
	$SQL = "DELETE FROM {friend_list}
			WHERE uid = %d AND lid = %d
	";
	$result = db_query($SQL,$user->uid,$lid);

	$SQL = "DELETE FROM {friend_list_member}
			WHERE lid = %d
	";
	db_query($SQL,$lid);

	// now delete any permission set for this list.
	$SQL = "DELETE FROM {friend_privacy}
			WHERE owner_id = %d AND list_id = %d
	";
	db_query($SQL,$user->uid,$lid);

	drupal_set_message(t('List removed along with all members.'));
	drupal_goto('friend');	

}
/************************************************************************
 *  remove member from list.  
 *
 * @param $lid
 * 	the id of the list
 * @return
 *	true
 */
function friendapi_lists_del_mem($lid=null,$uid=null){
	global $user;
	
	// is user owner of $lid?
	$SQL = "SELECT lid FROM {friend_list} WHERE lid = %d AND uid = %d";
	$record = db_fetch_object(db_query($SQL,$lid,$user->uid));

	if ($record->lid > 0){
		$SQL = "DELETE FROM {friend_list_member}
				WHERE lid = %d AND uid = %d
		";

		// delete member
		$result = db_query($SQL,$lid,$uid);
	};
	if ($result ){
		drupal_set_message(t("Friend removed from list"));
	};

	drupal_goto();
}
/************************************************************************
 *  return all members from a given friend list.
 *
 * @param $uid
 *	the owner of the list
 * @param $lid
 * 	the id of the list
 * @return
 *	returns all members of the list.
 */
function friendapi_list_member($uid,$lid=null){

    if ($lid != null){
        $SQL = "SELECT M.uid
                FROM {friend_list} L
                    INNER JOIN {friend_list_member} M on L.lid = M.lid
                WHERE L.uid = %d AND L.lid = %d
        ";
        $result = db_query($SQL,$uid,$lid);
        $output = "";
        while ($record = db_fetch_object($result)){
            $output .=  theme('friend_box', $record->uid,array('function_caller'=>'friendapi_list_member','current_list_id'=>$lid));
        };
    };

    return $output;
}

/************************************************************************
 * returns an array of friends.
 *
 * @param $uid
 * 	the user
 * @return
 *	an array of friends belonging to $uid
 */
function friendapi_friend_list($uid,$list_id=null){

	if ($list_id == null){
		$SQL = "SELECT U.* 
				FROM {users} U
					INNER JOIN {friend} F on U.uid = F.fid
				WHERE F.uid = %d
		";
	}else{
		$SQL = "SELECT U.* 
				FROM {users} U
					INNER JOIN {friend} F on U.uid = F.fid
				WHERE F.status = 1 AND F.uid = %d
					AND U.uid NOT IN (
						SELECT uid FROM {friend_list_member}
						WHERE lid = '$list_id'
					)
		";
	};
	$result = db_query($SQL,$uid);
	$friends = array();
	while ($record = db_fetch_object($result)){
		$friends[$record->uid] = $record->name;
	}

	return $friends;

}
/************************************************************************
 * returns the name of the list
 *
 * @param $list_id
 * 	the list id
 * @return
 *	name of the list
 */
function friendapi_list_name($list_id){

	$SQL = "SELECT list_name 
			FROM {friend_list}
			WHERE lid = %d
	";
	$record = db_fetch_object(db_query($SQL,$list_id));

	return $record->list_name;

}
/************************************************************************
 * check if $fid is friends with $uid
 *
 * @param $uid
 * 	the user
 * @param $fid
 * 	the friend
 * @return
 *	returns > 0 for true and 0 for false.
 */
function friendapi_is_friend($uid,$fid){

    $SQL = "SELECT COUNT(uid) as found
            FROM {friend}
            WHERE uid=%d AND fid=%d AND status=%d
    ";
    $record = db_fetch_object(db_query($SQL,$uid,$fid,FRIEND_FRIEND));

    return $record->found;
};
/************************************************************************
 * List all friends belonging to $uid
 *
 * @param $uid
 * 	friends of $uid
 * @return 
 *	list all friends belonging to $uid
 */
function friendapi_list($uid, $limit=20){

	$SQL = "SELECT F.fid,U.picture,U.name
			FROM {friend} F
				INNER JOIN {users} U on F.fid = U.uid
			WHERE F.uid = %d
				AND F.status = 1
	";
	$result = db_query_range($SQL,$uid,0,$limit);
	while($record = db_fetch_object($result)){
		$output .= theme('friend_profile_box',$record);
	};

	return array(
		'id'	=> 'friend_list',
		'title'	=> t('Friends'),
		'weight'	=> 1,
		'value' => $output,
	);

};
/************************************************************************
 * List all mutual friends belonging to $uid and current user viewing the profile
 *
 * @param $uid
 *  friends of $uid
 * @return 
 *  list all friends belonging to $uid
 */
function friendapi_mutual($uid, $limit=20){
global $user;

    $SQL = "SELECT O.fid, U.picture, U.name
            FROM {friend} O
					INNER JOIN {users} U on O.fid = U.uid
            WHERE O.uid = %d
				AND fid IN (
					SELECT I.fid FROM {friend} I 
						WHERE uid = %d AND I.status = 1
			)
    ";
    $result = db_query_range($SQL,$uid,$user->uid,0,$limit);
	$cnt =0; 
    while($record = db_fetch_object($result)){
		$cnt++;
        $output .= theme('friend_profile_box',$record);
    };
	if ($cnt == 0){
		$output .= t('No Mutual Friends found.');
	};

	return array(
		'id'	=> 'friend_mutual',
		'title'	=> t('Mutual Friends'),
		'weight'	=> 2,
		'value' => $output,
	);

};
/************************************************************************
 * List all groups belonging to $account
 * @param $account
 * @return 
 *  list all friends belonging to $uid
 */
function friendapi_og_groups($account){

	$groups = $account->og_groups;

	while(list($gid,$group) = each($groups)){
        $output .= theme('friend_og_groups',(object)$group);
	};

	return array(
		'id'	=> 'friend_groups',
		'title'	=> t('Groups'),
		'weight'	=> 3,
		'value' => $output,
	);

}
/************************************************************************
 * Display user info
 * @param $account
 * @return 
 *  list all friends belonging to $uid
 */
function friendapi_user_info($account){

	$output = $account->content['user_picture']['#value'];
	$output .= theme('username',$account);

	return array(
		'id'	=> 'friend_user_info',
		'weight'	=> $account->content['user_picture']['#weight'],
		'value' => $output,
	);
}
